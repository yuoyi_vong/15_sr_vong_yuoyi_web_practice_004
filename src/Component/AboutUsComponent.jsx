import React from "react";
import { Card } from "react-bootstrap";

export default function AboutUsComponent({ item }) {
  return (
    <div className="container mt-5 text-font">
      {item.map((temp2, i) =>
        i % 2 ? (
            <div className="row" key={i}>
            <div className="col-6">
              <Card.Body className="about-us-card-style">
                <Card.Text >{temp2.content}</Card.Text>
              </Card.Body>
            </div>
            <div className="col-6">
              <Card.Img
                variant="top"
                src={temp2.image}
              />
            </div>
          </div>
        ) : (
            <div className="row" key={i}>
            <div className="col-6">
              <Card.Img
                variant="top"
                src={temp2.image}
              />
            </div>
            <div className="col-6">
              <Card.Body className="about-us-card-style">
                <Card.Text>{temp2.content}</Card.Text>
              </Card.Body>
            </div>
          </div>
        )
      )}
    </div>
  );
}
