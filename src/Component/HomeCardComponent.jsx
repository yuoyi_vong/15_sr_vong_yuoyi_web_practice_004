import React from "react";
import { Card, Button, CardGroup, Col, Row } from "react-bootstrap";

export default function HomeCardComponent({ data }) {
  return (
    <div className="container mt-5">
      <h1 className="mb-3 text-font">
        <b>Trending Courses</b>
      </h1>
      <Row>
        {data.map((temp, i) => (
          <Col key={i}>
            <Card style={{ width: "18rem", textAlign: "center" }}>
              <Card.Img variant="top" src={temp.thumbnail}></Card.Img>
              <Card.Body className="text-font">
                <Card.Title>
                  <b>{temp.title}</b>
                </Card.Title>
                <Card.Text>{temp.description}</Card.Text>
                <Card.Text style={{ color: "red", fontSize: "24px" }}>
                  <b>{temp.price} $</b>
                </Card.Text>
                <Button variant="success">Buy the course</Button>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </div>
  );
}
