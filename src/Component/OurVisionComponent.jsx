import React from "react";
import { Row, Col, Card } from "react-bootstrap";
import "../CSS Style/myStyle.css";

export default function OurVisionComponent() {
  return (
    <div className="container mt-5">
      <Row xs={1} md={2} className="g-4 text-font">
          <Col>
            <Card>
              <Card.Body className="card-style">
                <Card.Title><b>Vision</b></Card.Title>
                <Card.Text>
                  <ul>
                      <li>To be the best SW Professional Training Center in Cambodia</li>
                  </ul>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card >
              <Card.Body className="card-style">
                <Card.Title><b>Mission</b></Card.Title>
                <Card.Text>
                  <ul>
                      <li>High quality training and research</li>
                      <li>Developing Capacity of SW Experts to be Leaders in IT Field</li>
                      <li>Developing sustainable ICT Program</li>
                  </ul>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card>
              <Card.Body className="card-style">
                <Card.Title><b>Strategy</b></Card.Title>
                <Card.Text>
                  <ul>
                      <li>Best training method with up to date curriculum and environment</li>
                      <li>Cooperation with the best IT industry to guarantee student's career and benefits</li>
                      <li>Additional Soft Skill, Management, Leadership training</li>
                  </ul>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card>
              <Card.Body className="card-style">
                <Card.Title><b>Slogan</b></Card.Title>
                <Card.Text>
                  <ul>
                      <li>"KSHRD, connects you to various opportunities in IT Field."</li>
                      <li>Raising brand awareness with continuous advertisement of SNS and any other media</li>
                  </ul>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
      </Row>
    </div>
  );
}
