import React from "react";
import { Navbar, Nav, Container } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import "../CSS Style/myStyle.css";

export default function NavBarComponent() {
  return (
    <div>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="#home" className="text-font">PT-004</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
            </Nav>
            <Nav className="text-font">
              <Nav.Link className="navbar-style" as={NavLink} to="/">Home</Nav.Link>
              <Nav.Link className="navbar-style" as={NavLink} to="/about" >About Us</Nav.Link>
              <Nav.Link className="navbar-style" as={NavLink} to="/vision" >Our Vision</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}
