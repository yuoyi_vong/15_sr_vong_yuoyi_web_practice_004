import React from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import "../CSS Style/myStyle.css";

export default function FooterComponent() {
  return (
    <div className="main-footer">
      <div className="container">
        <Row className="middle-position-text">
          <Col >
            <img
              src="https://kshrd.com.kh/static/media/logo.f368c431.png"
              style={{ width: "150px" }}
            />
            <p className="khmer-font">
              &copy; រក្សាសិទ្ធិគ្រប់យ៉ាងដោយ KSHRD Center ឆ្នាំ ២០២២
            </p>
          </Col>
          <Col>
            <h5>Address</h5>
            <p>
              <b>Address</b>: #12, St 323, Sangkat Boeung Kak II, KhanToul Kork,
              Phnom Penh, Cambodia
            </p>
          </Col>
          <Col>
            <h5>Contact</h5>
            <p>
              <b>Tel:</b> 012 998 919 (Khmer)
            </p>
            <p>
              <b>Tel:</b> 085 402 605 (Korean)
            </p>
            <p>
              <b>Email:</b> info.kshrd@gmail.com phirum.gm@gmail.com
            </p>
          </Col>
        </Row>
      </div>
    </div>
  );
}
